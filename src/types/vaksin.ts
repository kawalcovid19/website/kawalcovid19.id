export const PENELITIAN_VAKSIN_API_URL = 'https://menujuherdimmunity.id/api/penelitian';
export const KANDIDAT_VAKSIN_API_URL = 'https://menujuherdimmunity.id/api/kandidat';

export interface Penelitian {
  limitedApproval: number;
  phase1: number;
  phase2: number;
  phase3: number;
  preclinical: number;
  approved: number;
  lastUpdate: Date;
}

export interface PenelitianResponse {
  penelitianVaksin: Penelitian;
}

export interface Kandidat {
  id: string;
  tipeVaksin: string;
  dibuatOleh: string;
  fase: string;
  faseStatus: number;
}

export interface KandidatResponse {
  allKandidatVaksins: Kandidat[];
  lastUpdate: Date;
}
