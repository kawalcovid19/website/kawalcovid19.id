import styled from '@emotion/styled';
import { UnstyledAnchor } from 'components/design-system';

const PemerintahDaerahLink = styled(UnstyledAnchor)`
  text-decoration: none;

  &:hover,
  &:focus {
    h5 {
      text-decoration: underline;
    }
  }
`;

export default PemerintahDaerahLink;
