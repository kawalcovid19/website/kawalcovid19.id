import * as React from 'react';
import styled from '@emotion/styled';

import {
  Heading,
  themeProps,
  Text,
  Stack,
  UnorderedList,
  ListItem,
} from 'components/design-system';

const commonMapProps = {
  'aria-label': 'map',
  scrolling: 'no',
  style: { width: '100%', border: 'none' },
  height: '769',
};

const MapWilayah: React.FC = React.memo(() => (
  <iframe
    title="Indeks Kewaspadaan - KawalCovid19 - Indonesia (Kab/Ko) - 17 Mei 2021"
    id="datawrapper-chart-BA77E"
    src="https://datawrapper.dwcdn.net/BA77E/405/"
    {...commonMapProps}
  />
));

const KasusHarian = React.memo(() => (
  <iframe
    title="Total Kasus - KawalCovid19 - Indonesia (Kab/Kota) - 17 Mei 2021"
    id="datawrapper-chart-MwHOx"
    src="https://datawrapper.dwcdn.net/MwHOx/466/"
    {...commonMapProps}
  />
));

const KasusAktif = React.memo(() => (
  <iframe
    title="Kasus Aktif per 100.000 penduduk - KawalCovid19 - Indonesia (Kab/Kota) - 17 Mei 2021"
    id="datawrapper-chart-mkRJw"
    src="https://datawrapper.dwcdn.net/mkRJw/178/"
    {...commonMapProps}
  />
));

const TingkatKematian = React.memo(() => (
  <iframe
    title="Case Fatality Ratio - Kawalcovid19 -Indonesia (Kab/Ko) - 17 Mei 2021"
    id="datawrapper-chart-M2oHY"
    src="https://datawrapper.dwcdn.net/M2oHY/89/"
    {...commonMapProps}
  />
));

const RasioLacakIsolasi = React.memo(() => (
  <iframe
    title="RLI - KawalCovid19 - Indonesia (Kab/Ko) - 17 Mei 2021"
    id="datawrapper-chart-0p5op"
    src="https://datawrapper.dwcdn.net/0p5op/380/"
    {...commonMapProps}
  />
));

const SectionHeading = styled(Heading)`
  margin-bottom: ${themeProps.space.md}px;

  ${themeProps.mediaQueries.md} {
    margin-bottom: ${themeProps.space.xl}px;
  }
`;

const IndexKewaspadaan = React.memo(() => {
  React.useEffect(() => {
    window.addEventListener('message', function generate(a) {
      // eslint-disable-next-line no-void
      if (void 0 !== a.data['datawrapper-height'])
        // eslint-disable-next-line no-restricted-syntax, guard-for-in
        for (const e in a.data['datawrapper-height']) {
          const t =
            document.getElementById(`datawrapper-chart-${e}`) ||
            document.querySelector(`iframe[src*='${e}']`);
          // eslint-disable-next-line no-unused-expressions
          t && (t.style.height = `${a.data['datawrapper-height'][e]}px`);
        }
    });
  });
  return (
    <Stack>
      <SectionHeading>Seberapa besar risiko saya tertular di wilayah tertentu?</SectionHeading>
      <Text mb={4}>
        Indeks Kewaspadaan KawalCOVID19.id berupaya menjawab pertanyaan ini. Semakin tua warna merah
        di daerah tersebut, warga setempat dan orang yang akan bepergian ke situ perlu meningkatkan
        kewaspadaan dengan selalu menggunakan masker, menjaga jarak minimal dua meter, dan
        memperhatikan sirkulasi udara bila bertemu dengan orang lain.
      </Text>
      <MapWilayah />
      <Text>Indeks Kewaspadaan KawalCOVID19.id menggunakan berbagai indikator:</Text>
      <Text display="block" color="accents08" variant={700}>
        Kasus Harian
      </Text>
      <KasusHarian />
      <Text display="block" color="accents08" variant={700}>
        Kasus Aktif
      </Text>
      <KasusAktif />
      <Text display="block" color="accents08" variant={700}>
        Tingkat kematian untuk kasus positif COVID
      </Text>
      <TingkatKematian />
      <Text display="block" color="accents08" variant={700}>
        Rasio lacak isolasi
      </Text>
      <Text>
        Yaitu jumlah suspek dan kontak erat yang dibandingkan dengan jumlah kasus terkonfirmasi.
        Kami menggunakan target WHO agar 30 kontak erat ditelusuri dan dikarantina per kasus
        positif.
      </Text>
      <RasioLacakIsolasi />
      <Text>Selain itu kami menggunakan beberapa faktor tambahan:</Text>
      <UnorderedList>
        <ListItem>Tingkat kematian pada kasus probabel</ListItem>
        <ListItem>
          Seberapa banyak orang yang dites di suatu daerah dan proporsinya dengan jumlah penduduk di
          wilayah tersebut
        </ListItem>
        <ListItem>Tingkat kesembuhan pada Kasus Terkonfirmasi dan Kasus Probabel</ListItem>
        <ListItem>
          Jumlah Suspek dan Kontak Erat di satu daerah, atau yang kami sebut sebagai Rasio
          Lacak-Isolasi (RLI) yang menunjukkan besarnya upaya untuk menjaring orang-orang yang
          mungkin masih menulari komunitasnya.
        </ListItem>
        <ListItem>Jumlah penduduk pada suatu wilayah</ListItem>
      </UnorderedList>
      <Text as="p" my={10}>
        Untuk tiap faktor tersebut di atas, ada skor yang diberikan lalu dijumlahkan menjadi nilai
        relatif Indeks Kewaspadaan. Semakin tinggi skornya, semakin daerah tersebut perlu mewaspadai
        penyebaran COVID-19 di wilayahnya, termasuk risikonya menularkan ke daerah lain.
      </Text>
      <Text as="p" my={10}>
        Indeks Kewaspadaan ini divisualisasikan dalam peta yang akan memperlihatkan skor relatif per
        kabupaten untuk seluruh Indonesia. Bila kabupaten/kotamadyanya diklik, keseluruhan
        indikatornya terlihat sehingga apa langkah mitigasi mudah diketahui.
      </Text>
      <Text as="p" variant={200} my={10}>
        Peta Indeks Kewaspadaan KawalCOVID19.id diperbarui setiap hari.
      </Text>
    </Stack>
  );
});

export default IndexKewaspadaan;
