import * as React from 'react';
import styled from '@emotion/styled';
import { Box, themeProps } from 'components/design-system';

const GridWrapper = styled(Box)`
  display: grid;
  grid-template-columns: repeat(auto-fill, 1fr);
  grid-gap: 24px;

  ${themeProps.mediaQueries.md} {
    grid-template-columns: repeat(
      auto-fill,
      minmax(calc(${themeProps.widths.xl}px / 2 - 48px), 1fr)
    );
  }
`;

const HighlightGrid: React.FC = ({ children }) => {
  return <GridWrapper>{children}</GridWrapper>;
};

export default HighlightGrid;
