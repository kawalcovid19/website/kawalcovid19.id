import * as React from 'react';
import { Box, Stack, Text } from 'components/design-system';
import styled from '@emotion/styled';
import dynamic from 'next/dynamic';

const DetailGrid = styled(Box)`
  display: grid;
  grid-template-columns: 60px auto;
  grid-gap: 20px;
`;

interface DetailNumbersProps {
  embedToken: string;
  embedUrl: string;
  reportId: string;
}

const EmbeddedVisualizationNoSSR = dynamic(() => import('modules/kasus/EmbeddedVisualization'), {
  ssr: false,
});

const DetailNumbers: React.FC<DetailNumbersProps> = ({ embedToken, embedUrl, reportId }) => {
  const isActive = true;
  return (
    <Stack spacing="md">
      <DetailGrid>
        <EmbeddedVisualizationNoSSR
          id="meninggalAktif"
          embedToken={embedToken}
          embedUrl={embedUrl}
          reportId={reportId}
          currentPageName="meninggalAktif"
          height="100px"
          isActive={isActive}
        />
        <Box flex="1 1 auto">
          <Text display="block" color="accents07">
            Meninggal Positif
          </Text>
        </Box>
      </DetailGrid>
      <DetailGrid>
        <EmbeddedVisualizationNoSSR
          id="meninggalOdp"
          embedToken={embedToken}
          embedUrl={embedUrl}
          reportId={reportId}
          currentPageName="meninggalOdp"
          height="100px"
          isActive={isActive}
        />
        <Box flex="1 1 auto">
          <Text display="block" color="accents07">
            Meninggal ODP
          </Text>
          <Text display="block" color="accents07">
            (Orang Dalam Pengawasan)
          </Text>
        </Box>
      </DetailGrid>
      <DetailGrid display="flex" flexDirection="row">
        <EmbeddedVisualizationNoSSR
          id="meninggalPdp"
          embedToken={embedToken}
          embedUrl={embedUrl}
          reportId={reportId}
          currentPageName="meninggalPdp"
          height="100px"
          isActive={isActive}
        />
        <Box flex="1 1 auto">
          <Box>
            <Text display="block" color="accents07">
              Meninggal PDP
            </Text>
            <Text display="block" color="accents07">
              (Pasien Dalam Pengawasan)
            </Text>
          </Box>
        </Box>
      </DetailGrid>
    </Stack>
  );
};

export default DetailNumbers;
