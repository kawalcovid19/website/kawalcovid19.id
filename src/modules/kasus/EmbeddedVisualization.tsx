import * as React from 'react';
import { Box } from 'components/design-system';
import { IEmbedConfiguration } from 'powerbi-client';

interface EmbeddedVisualizationProps {
  id: string;
  currentPageName: string;
  embedToken: string;
  embedUrl: string;
  reportId: string;
  isActive: boolean;
  width?: string;
  height?: string;
}

const pageNameMapping = new Map<string, string>();
pageNameMapping.set('kematianDiverifikasi', 'ReportSection');
pageNameMapping.set('meninggalAktif', 'ReportSectiona1613b1f9b3859ef1ff4');
pageNameMapping.set('meninggalPdp', 'ReportSection53a5acc420decc00ca0c');
pageNameMapping.set('meninggalOdp', 'ReportSection6fb837cb08db97db5e0b');
pageNameMapping.set('provinsi', 'ReportSection0b0e5cdc042ecea90969');
pageNameMapping.set('usia', 'ReportSectionb8ba1c728bbb7ffc0932');
pageNameMapping.set('profesi', 'ReportSection440eacc7c4016a0b0ebb');

const EmbeddedVisualization: React.FC<EmbeddedVisualizationProps> = ({
  id,
  currentPageName,
  embedToken,
  embedUrl,
  reportId,
  isActive,
  width,
  height,
}) => {
  React.useEffect(() => {
    if (window !== undefined) {
      import('powerbi-client').then(r => {
        const { TokenType, BackgroundType } = r.models;
        const { factories, service } = r;
        const powerbi = new service.Service(
          factories.hpmFactory,
          factories.wpmpFactory,
          factories.routerFactory
        );
        const x = embedUrl;
        const embedConfiguration: IEmbedConfiguration = {
          type: 'report',
          id: reportId,
          embedUrl: x,
          tokenType: TokenType.Embed,
          accessToken: embedToken,
          pageName: pageNameMapping.get(currentPageName),
          settings: {
            background: BackgroundType.Transparent,
            navContentPaneEnabled: false,
          },
        };
        const el = document.getElementById(id);
        if (el !== null) {
          powerbi.reset(el);
        }
        if (reportId !== '' && el !== null && !el.hasChildNodes() && isActive) {
          el.setAttribute('powerbi-embed-url', embedUrl);
          const em = powerbi.embed(el, embedConfiguration);
          em.iframe.setAttribute('border', '0');
          em.iframe.setAttribute('frame-border', '0');
          em.iframe.setAttribute('scrolling', 'no');
          em.iframe.setAttribute(
            'style',
            `width: 100%; height: 100%; border-radius: 10px; overflow: hidden; border: 0;`
          );
        }
      });
    }
  });
  return embedToken && isActive ? (
    <>
      <Box id={id} width={width} height={height} overflow="hidden" />
    </>
  ) : (
    <Box>Loading...</Box>
  );
};

export default EmbeddedVisualization;
