import * as React from 'react';
import { GetStaticProps, NextPage } from 'next';
import styled from '@emotion/styled';
import { WordPressPost } from 'types/wp';
import convert from 'htmr';
import { PageWrapper, Content, Column } from 'components/layout';
import { Box } from 'components/design-system';
import { PostHeader } from 'modules/posts-index';
import { ResearchSection, CandidateTable } from 'modules/vaksin';
import { wp } from 'utils/api';
import ErrorPage from 'pages/_error';
import htmrTransform from 'modules/posts-index/utils/htmrTransform';

interface PostContentPageProps {
  post?: WordPressPost;
  errors?: string;
}

const ContentAsSection = Content.withComponent('section');

const Section = styled(ContentAsSection)`
  padding-bottom: 48px;
`;

const FAQPage: NextPage<PostContentPageProps> = ({ post }) => {
  if (!post) {
    return <ErrorPage statusCode={404} />;
  }
  return (
    <PageWrapper title={`Kenali Vaksin | KawalCOVID19`} pageTitle="Kenali Vaksin">
      <PostHeader
        type={post.type}
        title={post.title.rendered}
        description={post.excerpt.rendered}
      />
      <Section>
        <Column>
          <Box mb="xxl">
            {convert(post.content.rendered, {
              transform: htmrTransform,
            })}
          </Box>
          <ResearchSection />
          <CandidateTable />
        </Column>
      </Section>
    </PageWrapper>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  try {
    const post = await wp<WordPressPost>(`wp/v2/posts/1660`);

    if (post && post.id) {
      const props = { post } as PostContentPageProps;
      return { props };
    }

    throw new Error('Failed to fetch page');
  } catch (err) {
    const props = { errors: err.message } as PostContentPageProps;
    return { props };
  }
};

export default FAQPage;
