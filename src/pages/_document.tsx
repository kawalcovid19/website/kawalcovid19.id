/* eslint-disable react/no-danger */
import * as React from 'react';
import Document, { DocumentContext, Html, Head, Main, NextScript } from 'next/document';
import createEmotionServer from '@emotion/server/create-instance';
import * as Sentry from '@sentry/browser';
import emotionCache from 'utils/emotionCache';

const { extractCritical } = createEmotionServer(emotionCache);

interface ExtendedDocumentProps {
  ids: string[];
  css: string;
}

process.on('unhandledRejection', err => {
  Sentry.captureException(err);
});

process.on('uncaughtException', err => {
  Sentry.captureException(err);
});

export default class MyDocument extends Document<ExtendedDocumentProps> {
  static async getInitialProps(ctx: DocumentContext) {
    const { html, ...renderPageResult } = await ctx.renderPage();
    const { ids, css, ...props } = extractCritical(html);

    const initialProps = await Document.getInitialProps(ctx);

    return {
      ...initialProps,
      ...renderPageResult,
      ...props,
      styles: (
        <>
          {initialProps.styles}
          <style data-emotion-css={ids.join(' ')} dangerouslySetInnerHTML={{ __html: css }} />
        </>
      ),
    };
  }

  render() {
    return (
      <Html lang="id">
        <Head />
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
