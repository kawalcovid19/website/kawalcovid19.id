import * as React from 'react';
import { NextPage } from 'next';

import { PageWrapper, Content, Column } from 'components/layout';
import { Stack } from 'components/design-system';
import { IndexKewaspadaan } from 'modules/vaksin';

const Section = Content.withComponent('section');

const IndexPage: NextPage = () => (
  <PageWrapper pageTitle="Indeks Kewaspadaan">
    <Section>
      <Column>
        <Stack spacing="xxl">
          <IndexKewaspadaan />
        </Stack>
      </Column>
    </Section>
  </PageWrapper>
);

export default IndexPage;
