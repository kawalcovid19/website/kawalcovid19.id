import createCache from '@emotion/cache';

const emotionCache = createCache({
  key: 'kcov',
});

export default emotionCache;
