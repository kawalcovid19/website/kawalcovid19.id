export default function formatNumber(maybeNumber: unknown) {
  if (typeof maybeNumber === 'number') {
    return Intl.NumberFormat('id-ID').format(maybeNumber);
  }

  return undefined;
}
