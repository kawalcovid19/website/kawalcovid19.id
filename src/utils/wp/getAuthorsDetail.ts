import { wp } from 'utils/api';
import { WordPressUser } from 'types/wp';

async function getUser(id: number): Promise<WordPressUser> {
  const user = await wp<WordPressUser>(`wp/v2/users/${id}`, {
    _fields: 'id,name,description,slug,avatar_urls',
  });

  return user;
}

/** Get all auhor details given a list of author IDs */
export default async function getAuthorsDetail(authorIds: number[]) {
  const authors = await Promise.all(authorIds.map(author => getUser(author)));
  return authors;
}
