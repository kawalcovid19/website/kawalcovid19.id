import '@emotion/react';

declare module '@emotion/react' {
  export interface Theme {
    fonts: {
      system: string;
      systemMonospace: string;
      sansSerif: string;
      serif: string;
      monospace: string;
    };
    colors: {
      black: string;
      white: string;
      background: string;
      accents01: string;
      accents02: string;
      accents03: string;
      accents04: string;
      accents05: string;
      accents06: string;
      accents07: string;
      accents08: string;
      foreground: string;
      chart: string;
      brandred: string;
      brandgrey: string;
      error01: string;
      error02: string;
      error03: string;
      primary01: string;
      primary02: string;
      primary03: string;
      success01: string;
      success02: string;
      success03: string;
      warning01: string;
      warning02: string;
      warning03: string;
      highlight01: string;
      highlight02: string;
      highlight03: string;
      highlight04: string;
      hoverTable: string;
      progressBar: string;
      card: string;
      buttonlightmode: string;
      buttonlightmodetext: string;
      buttondarkmode: string;
      buttondarkmodetext: string;
      navgridbgmobile: string;
    };
    space: {
      xxxs: number;
      xxs: number;
      xs: number;
      sm: number;
      md: number;
      lg: number;
      xl: number;
      xxl: number;
    };
    breakpoints: string[];
    widths: {
      sm: number;
      md: number;
      lg: number;
      xl: number;
    };
    shadows: {
      single: string;
      double: string;
    };
  }
}
